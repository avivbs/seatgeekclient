import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var likeView: UIImageView!
    @IBOutlet var bigImageView: UIImageView!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    var presenter: DetailsPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(detailsView: self)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.likeViewClicked))
        likeView.addGestureRecognizer(gesture)
    }
    
    @objc func likeViewClicked() {
        presenter.likeClicked()
    }
    
}

extension DetailsViewController: DetailsPresenterView {
    
    func setEvent(event: Event) {
        titleLabel.text = event.title
        bigImageView.kf.setImage(with: event.imageURL, placeholder: UIImage(named: "event_placeholder"))
        locationLabel.text = event.location
        dateLabel.text = event.dateTime
        setLike(like: event.like)
    }
    
    func setLike(like: Bool) {
        likeView.image = UIImage(named: like ? "like_filled" : "like_empty")
    }
}

