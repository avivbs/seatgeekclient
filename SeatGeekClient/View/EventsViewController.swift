import UIKit
import Kingfisher

class EventsViewController: UIViewController {

    @IBOutlet var eventsTableView: UITableView!
    
    private let presenter = EventListPresenter()
    
    var dataSource = [Event]() {
        didSet {
            eventsTableView.reloadData()
        }
    }
    
    var selectedItem: (event: Event, indexPath: IndexPath)?
    
    struct Storyboard {
        static let detailsSegueID = "ShowDetailsSegue"
        static let eventCellID = "EventTableViewCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(eventsView: self)
        setupNavigationAndSearchBar()
    }

    func setupNavigationAndSearchBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsViewController = segue.destination as? DetailsViewController, let event = selectedItem?.event {
            detailsViewController.presenter = DetailsPresenter(event) { [weak self] in
                guard let indexPath = self?.selectedItem?.indexPath else { return }
                self!.eventsTableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter.cancelSearch()
    }
    
    deinit {
        presenter.detach()
    }

}

extension EventsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.eventCellID, for: indexPath) as! EventTableViewCell
        let event = dataSource[indexPath.row]
        cell.titleLabel.text = event.title
        cell.locationLabel.text = event.location
        cell.dateTimeLabel.text = event.dateTime
        cell.photoView.kf.setImage(with: event.imageURL, placeholder: presenter.placeholderImage)
        cell.likeView.isHidden = !event.like
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedItem = (dataSource[indexPath.row], indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: Storyboard.detailsSegueID, sender: nil)
    }
}

extension EventsViewController: EventsPresenterView {
    
    func updateEvents(events: [Event]) {
        self.dataSource = events
    }
    
}

extension EventsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchTextUpdated(searchText)
    }
    
}

class EventTableViewCell: UITableViewCell {
    @IBOutlet var photoView: UIImageView!
    @IBOutlet var likeView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var dateTimeLabel: UILabel!
}
