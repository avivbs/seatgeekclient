import Foundation

protocol DetailsPresenterView: class {
    func setEvent(event: Event)
    func setLike(like: Bool)
}

class DetailsPresenter {
    
    weak private var detailsPresenterView: DetailsPresenterView?
    var event: Event!
    var onLikeAction: (() -> Void)?
    
    init(_ event: Event, onLikeAction: (() -> Void)? = nil) {
        self.event = event
        self.onLikeAction = onLikeAction
    }
    
    func attach(detailsView: DetailsPresenterView) {
        self.detailsPresenterView = detailsView
        detailsPresenterView?.setEvent(event: event)
    }
    
    func likeClicked() {
        event.like = !event.like
        if event.like {
            LocalStorageManager.sharedInstance.addLike(eventID: event.id)
        } else {
            LocalStorageManager.sharedInstance.removeLike(eventID: event.id)
        }
        detailsPresenterView?.setLike(like: event.like)
        onLikeAction?()
    }
    
    func detach() {
        self.detailsPresenterView = nil
    }
    
}
