import Foundation
import UIKit

protocol EventsPresenterView: class {
    func updateEvents(events: [Event])
}

class EventListPresenter {
    
    static let queryMinimumLength = 3
    
    let placeholderImage = UIImage(named: "event_placeholder")
    
    weak private var eventsPresenterView: EventsPresenterView?
    var searchManager = SearchManager(throttlingInterval: 0.4)

    func attach(eventsView: EventsPresenterView) {
        self.eventsPresenterView = eventsView
        self.searchManager.delegate = self
    }
    
    func searchTextUpdated(_ text: String) {
        guard text.count >= EventListPresenter.queryMinimumLength else {
            cancelSearch()
            clearEvents()
            return
        }
        searchManager.searchEvents(text)
    }
    
    func cancelSearch() {
        searchManager.cancelRequest(endpoint: SeatGeek.eventsEndpoint)
    }
    
    func clearEvents() {
        eventsPresenterView?.updateEvents(events: [Event]())
    }
    
    func detach() {
        self.eventsPresenterView = nil
    }
}

extension EventListPresenter: SearchDelegate {
    
    func eventsUpdated(events: [Event]) {
        if !events.isEmpty {
            eventsPresenterView?.updateEvents(events: events)
        }
    }
    
}
