import Foundation

class LocalStorageManager {
    
    static let likesArrayKey = "likesArrayKey"
    static let sharedInstance = LocalStorageManager()
    
    lazy var likes: Set<String> = {
       return loadLikes()
    }()
    
    private func loadLikes() -> Set<String> {
        guard let likes = UserDefaults.standard.stringArray(forKey: LocalStorageManager.likesArrayKey) else {
            return Set<String>()
        }
        return Set(likes.map { $0 })
    }
    
    func isLiked(eventID id: String) -> Bool {
        return likes.contains(id)
    }
    
    func addLike(eventID id: String) {
        likes.insert(id)
    }
    
    func removeLike(eventID id: String) {
        likes.remove(id)
    }
    
    func saveLikes() {
        UserDefaults.standard.set(Array(likes), forKey: LocalStorageManager.likesArrayKey)
    }
    
    deinit {
        saveLikes()
    }
}
