
import Foundation
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

typealias APIResult = (error: String?, json: JSON?)
typealias APICallback = ((APIResult) -> Void)?

class HTTPClient {
    
    class func request(method: HTTPMethod, url: String, urlParam: String? = nil, queryParams: [String : String]? = nil, callback: APICallback) -> DataRequest {
        return Alamofire.request(url, method: method, parameters: queryParams, encoding: encoding(for: method)).validate().responseSwiftyJSON { response in
            handleResponse(response, callback: callback)
            }.responseString { log in
                print(log)
        }
    }
    
    fileprivate class func handleResponse(_ response: DataResponse<JSON>, callback: APICallback) {
        var result: APIResult? = nil
        switch response.result {
        case .success(let json):
            result = (nil, json)
            
        case .failure(let error):
            result = ("\(error)", nil)
        }
        callback?(result!)
    }
    
    fileprivate class func encoding(for method: HTTPMethod) -> ParameterEncoding {
        switch method {
        case .get, .delete:
            return URLEncoding.queryString
        default:
            return URLEncoding.default
        }
    }
}
