import Foundation
import SwiftyJSON

class Event {
    
    var id: String!
    var title: String!
    var location: String?
    var date: Date?
    var imageURL: URL?
    var like = false
    
    var dateTime: String {
        return date?.formatForDisplay() ?? ""
    }
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.title = json["title"].stringValue
        if let dateString = json["datetime_utc"].string {
            self.date = Utils.extractDate(from: dateString, format: SeatGeek.dateFormat)
        }
        self.location = json["venue"]["display_location"].string
        if let performer = json["performers"].array?.first, let imageURL = performer["image"].url {
            self.imageURL = imageURL
        }
        like = LocalStorageManager.sharedInstance.isLiked(eventID: self.id)
    }
    
}
