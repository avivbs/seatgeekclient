import Foundation
import Alamofire

protocol SearchDelegate: class {
    func eventsUpdated(events: [Event])
}

struct SeatGeek {
    static let apiKey = "MTI5MDExOTF8MTUzNTQ2Mzk5Mi40Mw"
    static let baseURL = "https://api.seatgeek.com/2/"
    static let defaultQueryParams = ["client_id" : apiKey]
    static let eventsEndpoint = "events"
    static let dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
}

class SearchManager {
    
    weak var delegate: SearchDelegate?
    private var currentRequests = [String : DataRequest]()
    private var throttler: Throttler? = nil
    
    init(throttlingInterval: Float? = nil) {
        if let interval = throttlingInterval {
            throttler = Throttler(seconds: interval)
        }
    }
    
    func searchEvents(_ text: String) {
        let endpoint = SeatGeek.eventsEndpoint
        guard let throttler = self.throttler else {
            requestEvents(endpoint, queryText: text)
            return
        }
        throttler.throttle {
            self.requestEvents(endpoint, queryText: text)
        }
    }
    
    func requestEvents(_ endpoint: String, queryText: String) {
        self.currentRequests[endpoint]?.cancel()
        let eventSearchURL = SeatGeek.baseURL + endpoint
        let queryParams = ["q" : queryText].merging(SeatGeek.defaultQueryParams) { $1 }
        self.currentRequests[endpoint] = HTTPClient.request(method: .get, url: eventSearchURL, queryParams: queryParams) { [weak self] error, json in
            self?.currentRequests.removeValue(forKey: endpoint)
            guard error == nil, let json = json else { return }
            let eventsJsonArray = json["events"].arrayValue
            var events = [Event]()
            eventsJsonArray.forEach { events.append(Event(json: $0)) }
            self?.delegate?.eventsUpdated(events: events)
        }
    }
    
    func cancelRequest(endpoint: String) {
        self.currentRequests[endpoint]?.cancel()
    }
    
}
