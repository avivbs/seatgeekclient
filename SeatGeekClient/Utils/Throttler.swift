//  Throttler.swift
//
//  Created by Daniele Margutti on 10/19/2017
//
//  web: http://www.danielemargutti.com
//  email: hello@danielemargutti.com
//
//  Updated by Aviv to receive qos and interval as Float 30/08/18

import UIKit
import Foundation

public class Throttler {
    
    private let maxInterval: Float
    private let queue: DispatchQueue
    private var job: DispatchWorkItem = DispatchWorkItem(block: {})
    private var previousRun: Date = Date.distantPast
    
    init(seconds: Float, qos: DispatchQoS.QoSClass = .background) {
        self.maxInterval = seconds
        self.queue = DispatchQueue.global(qos: qos)
    }
    
    func throttle(block: @escaping () -> ()) {
        job.cancel()
        job = DispatchWorkItem(){ [weak self] in
            self?.previousRun = Date()
            block()
        }
        let delay = Date.second(from: previousRun) > maxInterval ? 0 : maxInterval
        queue.asyncAfter(deadline: .now() + Double(delay), execute: job)
    }
}

private extension Date {
    static func second(from referenceDate: Date) -> Float {
        return Float(Date().timeIntervalSince(referenceDate))
    }
}
